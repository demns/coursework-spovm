﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web;



namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void GetTxtHtml()
        {
            HttpWebRequest wReq;
            HttpWebResponse wResp;
            Stream rStream;
            txtHeader.Text = "";
            // Place the web request to the server by specifying the URL
            try
            {
                IdnMapping idn = new IdnMapping();
                Uri uriIdn;
                if (txtUrl.Text.IndexOf("http") < 0)
                    uriIdn = new Uri("http://" + txtUrl.Text);
                else 
                    uriIdn = new Uri(txtUrl.Text);

                UriBuilder uriBuilder = new UriBuilder(uriIdn);

                WebRequest wr = WebRequest.Create(uriBuilder.Uri);
                uriBuilder.Host = idn.GetAscii(uriBuilder.Host);
                    wReq = (HttpWebRequest)wr;

                wReq.KeepAlive = true;// For some reason need for a persistant connection
                wReq.Referer = txtReferrer.Text;// The link that referred us to the URL
                wReq.UserAgent = txtUserAgent.Text;// The user agent of the browser
                wResp = (HttpWebResponse)wReq.GetResponse();// Get the response from the server
                txtHeader.Text = wResp.Headers.ToString();// Display the header of the response
                rStream = wResp.GetResponseStream();// Get a stream to read the body of the response

                string strFile = WriteToFile(rStream);

                string[] contentStrings = ForFile.AllTags(strFile);

                StackPanel1.Children.Clear();

                string s = String.Empty;
                for (int i = 0; i< contentStrings.Length; i++)
                {
                    s = contentStrings[i];
                    try
                    {
                        if (s == null) break;
                        if (s.IndexOf("og:image") >= 0 || 
                            (s.IndexOf("img") >= 0 && s.IndexOf("src") >= 0))
                        {
                            int begin = s.IndexOf("http", 0);
                            int end = s.IndexOf("\"", begin);
                            string way = s.Substring(begin, end - begin);
                            var img = new Image();
                            BitmapImage logo = new BitmapImage();

                            int width = 100;
                            int height = 100;
                            begin = s.IndexOf("width", 0);
                            if (begin > 0)
                            {
                                for (; (s[begin] < '0' || s[begin] > '9') && begin < s.Length; begin++) ;
                                if (!int.TryParse(s.Substring(begin, 4), out width))
                                    if (!int.TryParse(s.Substring(begin, 3), out width))
                                        if (!int.TryParse(s.Substring(begin, 2), out width))
                                            width = 100;
                            }
                            begin = s.IndexOf("height", 0);
                            if (begin > 0)
                            {
                                for (; (s[begin] < '0' || s[begin] > '9') && begin < s.Length; begin++) ;
                                if (!int.TryParse(s.Substring(begin, 4), out height))
                                    if (!int.TryParse(s.Substring(begin, 3), out height))
                                        if (!int.TryParse(s.Substring(begin, 2), out height))
                                            height = 100;
                            }
                            if (width < 8 || height < 8) break;
                            try
                            {
                                logo.BeginInit();
                                logo.UriSource = new Uri(way);
                                logo.EndInit();
                                img.Source = logo;
                                img.Height = height;
                                img.Width = width;
                                StackPanel1.Children.Add(img);
                            }
                            catch (Exception e)
                            {
                                WarningBox.Text += e.Message + " in trying to show picture" + way;
                            }
                        }

                        else
                        {
                            TextBox t = new TextBox();
                            t.IsReadOnly = true;
                            
                            t.TextWrapping = TextWrapping.Wrap;
                            t.BorderBrush = Brushes.White;
                            
                            if (s.IndexOf("a href") >= 0)
                            {
                                t.ToolTip = s;
                                int begin = s.IndexOf("\"", 0);
                                int end = s.IndexOf("\"", begin + 1);
                                t.ToolTip = s.Substring(begin + 1, end - begin - 1);
                                s = contentStrings[++i];
                                if (s.IndexOf("og:image") >= 0 ||
                            (s.IndexOf("img") >= 0 && s.IndexOf("src") >= 0))
                                    continue;
                                t.FontWeight = FontWeights.ExtraBlack;
                            }


                            if (s.IndexOf("a href") >= 0)
                            {
                                int begin = s.IndexOf(">", 0);
                                if (begin>0) t.Text = s.Substring(begin+1, s.Length-begin);
                            }
                            else
                                t.Text = s;

                            t.SelectionChanged += delegate(object sender, RoutedEventArgs e)
                            {
                                if (t.ToolTip != null)
                                {
                                    if (t.ToolTip.ToString().IndexOf("http") < 0)
                                    {
                                        if (txtUrl.Text.IndexOf("/", 0) < 0)
                                            txtUrl.Text = txtUrl.Text + t.ToolTip.ToString();
                                        else
                                        {
                                            if (txtUrl.Text.IndexOf("http", 0) >= 0)
                                            {
                                                int begin = txtUrl.Text.IndexOf("/", 0);
                                                begin++;
                                                begin = txtUrl.Text.IndexOf("/", begin);
                                                begin++;
                                                begin = txtUrl.Text.IndexOf("/", begin);
                                                if (begin > 0) txtUrl.Text = txtUrl.Text.Substring(0, begin);
                                                txtUrl.Text += t.ToolTip.ToString();
                                            }
                                            else
                                            {
                                                int begin = txtUrl.Text.IndexOf("/", 0);
                                                if (begin>0) 
                                                    txtUrl.Text = txtUrl.Text.Substring(0, begin);
                                                txtUrl.Text += t.ToolTip.ToString();
                                            }
                                        }
                                    }
                                    else
                                        txtUrl.Text = t.ToolTip.ToString();
                                }
                                    GetTxtHtml();
                            };

                            t.MaxLines = t.Text.Length / 100 + 1;
                            StackPanel1.Children.Add(t);
                        }
                    }
                    catch (Exception e)
                    {
                        WarningBox.Text += e.Message + " in screen going section" + s;
                    }
                }
            }

            catch (Exception e)
            {
                txtHeader.Text = "Get an error trying to connect, because: " + e.Message;
            }
        }

        private string WriteToFile(Stream rStream)
        {
            int bufCount = 0;// Needed for looping through the buffer
            // Buffer in which we're going to store data coming from the server
            byte[] byteBuf = new byte[1020000];
            // Loop as long as there's data in the buffer
            string wwwUrl = txtUrl.Text;
            string[] wwwUrlParse = wwwUrl.Split('/', '\\', '.');
            if (wwwUrl.IndexOf("http") < 0)
                wwwUrl = wwwUrlParse[0];
            else
                wwwUrl = wwwUrlParse[2];



            System.IO.Directory.CreateDirectory("Content");
            if (System.IO.File.Exists("Content\\" + wwwUrl + ".txt"))
                System.IO.File.Delete("Content\\" + wwwUrl + ".txt");
            FileInfo fi1 = new FileInfo("Content\\" + wwwUrl + ".txt");


            if (!fi1.Exists)
            {
                using (StreamWriter sw = fi1.CreateText())
                {
                    do
                    {// Fill the buffer with data
                        bufCount = rStream.Read(byteBuf, 0, byteBuf.Length);
                        if (bufCount != 0)
                            sw.WriteLine(Encoding.UTF8.GetString(byteBuf, 0, bufCount));
                    }
                    while (bufCount > 0);
                }
            }
            return "Content\\" + wwwUrl + ".txt";
        }

        private void AdressEnter(object sender, KeyEventArgs e)
        {
            if (e.Key.ToString().CompareTo("Return") == 0)

                try
                {
                    GetTxtHtml();
                }
                catch(Exception ex)
                {
                    MessageBox.Show ("Ops. It's very bad." + ex.Message);
                }
            else return;
        }

        private void LentaButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "lenta.ru";
            GetTxtHtml();
        }

        private void RiaButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "ria.ru";
            GetTxtHtml();
        }

        private void HabrahabrButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "habrahabr.ru";
            GetTxtHtml();
        }

        private void TutButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "tut.by";
            GetTxtHtml();
        }

        private void WikiButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "en.wikipedia.org/wiki/Main_Page";
            GetTxtHtml();
        }

        private void RamblerButton_Click(object sender, RoutedEventArgs e)
        {
            txtUrl.Text = "rambler.ru";
            GetTxtHtml();
        }

    }

    public class ForFile
    {
        public static string[] GetContent(string strFile)
        {
            string fileToStr = string.Empty; //all file placed here
            string[] content = new string[2000]; // string for links
            int numberOfContents = 0;

            fileToStr = File.ReadAllText(strFile, System.Text.Encoding.UTF8);

            int index_begin = 0;
            int index_end = 0;
            for (; ; )
            {
                try
                {
                    index_begin = fileToStr.IndexOf("<p>", ++index_begin);
                    index_end = fileToStr.IndexOf("</p>", index_begin);
                }
                catch { return content; }


                if (index_begin >= 0 && index_end >= 0)
                    content[numberOfContents++] = fileToStr.Substring(index_begin, index_end - index_begin + 8);
                else break;
            }
            return content;
        }

        public static string[] AllTags(string strFile)
        {
            string fileToStr = string.Empty; //all file placed here
            string[] content = new string[2000]; // string for links
            int numberOfContents = 0;
            string currentTag = string.Empty;

            fileToStr = File.ReadAllText(strFile, System.Text.Encoding.UTF8);

            var stackForTags = new Stack<string>();

            bool isImage = true;
            for (int i = 0; i < fileToStr.Length && numberOfContents < 1750; i++)
            {
                try
                {
                    if (currentTag.IndexOf("og:image") >= 0 && isImage)
                    {
                        content[numberOfContents++] = currentTag;
                        isImage = false;
                    }
                    
                    

                    if (fileToStr[i] == '<')
                    {
                        int index_end;
                        index_end = fileToStr.IndexOf(">", i);
                        if (index_end > 0)
                            currentTag = fileToStr.Substring(i + 1, index_end - i);
                        else continue;
                        i += index_end - i;
                    }
                    else

                        if (fileToStr[i] == '\n' || fileToStr[i] == '\r') continue;

                        else
                        {
                            int index_end;
                            index_end = fileToStr.IndexOf("<", i);
                            if (index_end < 0) continue;
                            if (System.Text.RegularExpressions.Regex.IsMatch
                               (fileToStr.Substring(i, index_end - i), "[а-яА-Яa-zA-Z]") 
                               && currentTag.IndexOf("script") < 0)
                            {
                                try
                                {
                                    if (currentTag.IndexOf("a href") >= 0)
                                        content[numberOfContents++] = currentTag;
                                    if (numberOfContents > 0 && content[numberOfContents - 1].Length < 100 &&
                                        (index_end - i) < 15)
                                        content[numberOfContents - 1] = string.Concat(content[numberOfContents - 1], "   ", fileToStr.Substring(i, index_end - i));
                                    else
                                        content[numberOfContents++] = fileToStr.Substring(i, index_end - i);
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show(e.Message + "in parsing text section");
                                }
                            }
                            i += index_end - i - 1;
                        }

                    if (currentTag.IndexOf("img ") >= 0)
                        content[numberOfContents++] = currentTag;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message + " in parsing section");
                }
            }

            return content;
        }
    }
}
